package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Arugula;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.AsiagoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Chicory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChilliSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Kale;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.ManilaClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Parsley;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.SweetDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Tatsoi;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new SweetDough();
    }
    
    public Sauce createSauce() {
        return new ChilliSauce();
    }

    public Cheese createCheese() {
        return new AsiagoCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new Arugula(),new Chicory(),new Kale(),new Parsley(),new Tatsoi()};
        return veggies;
    }

    public Clams createClam() {
        return new ManilaClams();
    }
}
