package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Arugula;

public class ArugulaTest {
	@Test
    public void testToString() {
		Arugula veggies = new Arugula();
        String equal = "Arugula";
        assertEquals(equal, veggies.toString());
    }
}
