package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;

public class MushroomTest {
	@Test
    public void testToString() {
		Mushroom veggies = new Mushroom();
        String equal = "Mushrooms";
        assertEquals(equal, veggies.toString());
    }
}
