package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;

public class ThickCrustDoughTest {
	@Test
    public void testToString() {
		ThickCrustDough dough = new ThickCrustDough();
        String equal = "ThickCrust style extra thick crust dough";
        assertEquals(equal, dough.toString());
    }
}
