package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Tatsoi;

public class TatsoiTest {
	@Test
    public void testToString() {
		Tatsoi veggies = new Tatsoi();
        String equal = "Tatsoi";
        assertEquals(equal, veggies.toString());
    }
}
