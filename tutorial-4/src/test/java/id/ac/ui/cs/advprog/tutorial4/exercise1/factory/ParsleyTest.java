package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Parsley;

public class ParsleyTest {
	@Test
    public void testToString() {
		Parsley veggies = new Parsley();
        String equal = "Parsley";
        assertEquals(equal, veggies.toString());
    }
}
