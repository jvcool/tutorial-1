package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;

public class ReggianoCheeseTest {
	@Test
    public void testToString() {
		ReggianoCheese cheese = new ReggianoCheese();
        String equal = "Reggiano Cheese";
        assertEquals(equal, cheese.toString());
    }
}
