package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;

public class BlackOlivesTest {
	@Test
    public void testToString() {
		BlackOlives veggies = new BlackOlives();
        String equal = "Black Olives";
        assertEquals(equal, veggies.toString());
    }
}
