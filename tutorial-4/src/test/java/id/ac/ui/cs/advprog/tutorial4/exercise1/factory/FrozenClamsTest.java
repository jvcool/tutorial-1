package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;

public class FrozenClamsTest {
	@Test
    public void testToString() {
		FrozenClams clams = new FrozenClams();
        String equal = "Frozen Clams from Chesapeake Bay";
        assertEquals(equal, clams.toString());
    }
}
