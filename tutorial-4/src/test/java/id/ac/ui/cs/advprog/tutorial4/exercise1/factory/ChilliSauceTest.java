package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChilliSauce;

public class ChilliSauceTest {
	@Test
    public void testToString() {
		ChilliSauce sauce = new ChilliSauce();
        String equal = "Chilli Sauce";
        assertEquals(equal, sauce.toString());
    }
}
