package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.SweetDough;

public class SweetDoughTest {
	@Test
    public void testToString() {
		SweetDough dough = new SweetDough();
        String equal = "Sweet Dough";
        assertEquals(equal, dough.toString());
    }
}
