package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;

public class ParmesanCheeseTest {
	@Test
    public void testToString() {
		ParmesanCheese cheese = new ParmesanCheese();
        String equal = "Shredded Parmesan";
        assertEquals(equal, cheese.toString());
    }
}
