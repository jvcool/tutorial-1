package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;

public class GarlicTest {
	@Test
    public void testToString() {
		Garlic veggies = new Garlic();
        String equal = "Garlic";
        assertEquals(equal, veggies.toString());
    }
}
