package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Kale;

public class KaleTest {
	@Test
    public void testToString() {
		Kale veggies = new Kale();
        String equal = "Kale";
        assertEquals(equal, veggies.toString());
    }
}
