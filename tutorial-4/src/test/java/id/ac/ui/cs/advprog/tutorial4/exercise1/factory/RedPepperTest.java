package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;

public class RedPepperTest {
	@Test
    public void testToString() {
		RedPepper veggies = new RedPepper();
        String equal = "Red Pepper";
        assertEquals(equal, veggies.toString());
    }
}
