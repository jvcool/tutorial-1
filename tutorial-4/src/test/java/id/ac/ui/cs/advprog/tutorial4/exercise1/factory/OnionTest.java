package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;

public class OnionTest {
	@Test
    public void testToString() {
		Onion veggies = new Onion();
        String equal = "Onion";
        assertEquals(equal, veggies.toString());
    }
}
