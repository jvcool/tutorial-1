package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;

public class SpinachTest {
	@Test
    public void testToString() {
		Spinach veggies = new Spinach();
        String equal = "Spinach";
        assertEquals(equal, veggies.toString());
    }
}
