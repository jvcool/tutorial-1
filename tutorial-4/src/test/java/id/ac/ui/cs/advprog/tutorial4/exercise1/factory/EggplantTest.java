package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;

public class EggplantTest {
	@Test
    public void testToString() {
		Eggplant veggies = new Eggplant();
        String equal = "Eggplant";
        assertEquals(equal, veggies.toString());
    }
}
