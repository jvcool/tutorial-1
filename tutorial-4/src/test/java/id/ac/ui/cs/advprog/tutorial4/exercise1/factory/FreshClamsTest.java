package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;

public class FreshClamsTest {
	@Test
    public void testToString() {
		FreshClams clams = new FreshClams();
        String equal = "Fresh Clams from Long Island Sound";
        assertEquals(equal, clams.toString());
    }
}
