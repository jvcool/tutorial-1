package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.ManilaClams;

public class ManilaClamsTest {
	@Test
    public void testToString() {
		ManilaClams clams = new ManilaClams();
        String equal = "Manila Clams from Manila";
        assertEquals(equal, clams.toString());
    }
}
