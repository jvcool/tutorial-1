package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Chicory;

public class ChicoryTest {
	@Test
    public void testToString() {
		Chicory veggies = new Chicory();
        String equal = "Chicory";
        assertEquals(equal, veggies.toString());
    }
}
