package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.AsiagoCheese;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class AsiagoCheeseTest {
	@Test
    public void testToString() {
		AsiagoCheese cheese = new AsiagoCheese();
        String equal = "Asiago";
        assertEquals(equal, cheese.toString());
    }

}
