package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;

public class MozzarellaCheeseTest {
	@Test
    public void testToString() {
		MozzarellaCheese cheese = new MozzarellaCheese();
        String equal = "Shredded Mozzarella";
        assertEquals(equal, cheese.toString());
    }
}
