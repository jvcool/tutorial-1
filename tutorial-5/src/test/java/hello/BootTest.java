package hello;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = Application.class)
public class BootTest {
    @Test
    public void test() {
        Application.main(new String[]{"--spring.main.web-environment=false"});
    }
}
