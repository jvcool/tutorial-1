package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import id.ac.ui.cs.advprog.tutorial2.exercise1.receiver.CeilingFan;

public class CeilingFanOffCommand extends CeilingFanCommand {

	public CeilingFanOffCommand(CeilingFan ceilingFan) {
		super(ceilingFan);
	}

	protected void operate() {
		// TODO Complete me!
		ceilingFan.off();
	}
}
