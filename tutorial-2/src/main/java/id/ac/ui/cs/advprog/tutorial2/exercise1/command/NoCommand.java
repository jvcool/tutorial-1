package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

public class NoCommand implements Command {

	public void execute() {
		// Do nothing
	}

	public void undo() {
		// Do nothing
	}
}
