package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

public class MacroCommand implements Command {

	private List<Command> commands;

	public MacroCommand(Command[] commands) {
		this.commands = Arrays.asList(commands);
	}

	public void execute() {
		// for (Command command : commands) {
		// command.execute();
		// }
		this.commands.stream().forEach(elem -> elem.execute());

	}

	public void undo() {
		// TODO Complete me!
		// for (int i = commands.size() - 1; i >= 0; i--) {
		// Command command = commands.get(i);
		// command.undo();
		// }
		List<Command> reverseCommands = this.commands;
		Collections.reverse(reverseCommands);
		reverseCommands.stream().forEach(elem -> elem.undo());

	}
}
