package id.ac.ui.cs.advprog.tutorial3.composite.higherups;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        //TODO Implement
		if(salary < 200000){
			throw new IllegalArgumentException();
		}
    	this.name = name; 
    	this.salary = salary;
    }
    @Override
    public double getSalary() {
        //TODO Implement
    	return this.salary;
    }
    public String getRole() {
        return "CEO";
    }
}
